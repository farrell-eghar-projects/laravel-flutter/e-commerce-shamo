<?php

namespace App\Http\Controllers\API;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $id = $request->input('id');
        $limit = $request->input('limit', 6);
        $name = $request->input('name');
        $description = $request->input('description');
        $tags = $request->input('tags');
        $category = $request->input('category');
        $price_from = $request->input('price_from');
        $price_to = $request->input('price_to');
        
        if ($id) {
            $products = Product::with(['category', 'productGalleries'])->find($id);
            if ($products) {
                return ResponseFormatter::success(
                    $products,
                    'Data produk berhasil diambil'
                );
            } else {
                return ResponseFormatter::error(
                    null,
                    'Data produk tidak ditemukan',
                    404
                );
            }
        }
        
        $products = Product::with(['category', 'productGalleries']);
        
        if ($name) {
            $products->where('name', 'like', '%' . $name . '%');
        }
        
        if ($description) {
            $products->where('description', 'like', '%' . $description . '%');
        }
        
        if ($tags) {
            $products->where('tags', 'like', '%' . $tags . '%');
        }
        
        if ($price_from) {
            $products->where('price', '>=', $price_from);
        }
        
        if ($price_to) {
            $products->where('price', '<=', $price_to);
        }
        
        if ($category) {
            $products->where('category', $category);
        }
        
        return ResponseFormatter::success(
            $products->paginate($limit),
            'Data produk berhasil diambil'
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
