<?php

namespace App\Http\Controllers\API;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Transaction\TransactionCheckoutRequest;
use App\Models\Transaction;
use App\Models\TransactionItem;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sanctum');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $id = $request->input('id');
        $limit = $request->input('limit', 6);
        $status = $request->input('status');
        
        if ($id) {
            $transactions = Transaction::with('transactionItems.product')->find($id);
            if ($transactions) {
                return ResponseFormatter::success(
                    $transactions,
                    'Data transaksi berhasil diambil',
                );
            } else {
                return ResponseFormatter::error(
                    null,
                    'Data transakti tidak ditemukan',
                    404
                );
            }
        }
        
        $transactions = Transaction::with('transactionItems.product')->where('user_id', auth()->id());
        
        if ($status) {
            $transactions->where('status', $status);
        }
        
        return ResponseFormatter::success(
            $transactions->paginate($limit),
            'Data transaksi berhasil diambil',
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TransactionCheckoutRequest $request)
    {
        $transaction = Transaction::create([
            'user_id' => auth()->id(),
            'address' => $request->address,
            'total_price' => $request->total_price,
            'shipping_price' => $request->shipping_price,
            'status' => $request->status,
        ]);
        
        foreach ($request->items as $product) {
            TransactionItem::create([
                'user_id' => auth()->id(),
                'product_id' => $product['id'],
                'transaction_id' => $transaction->id,
                'quantity' => $product['quantity']
            ]);
        }
        
        return ResponseFormatter::success(
            $transaction->load('transactionItems.product'),
            'Transaksi berhasil'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
