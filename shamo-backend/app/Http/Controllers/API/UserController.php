<?php

namespace App\Http\Controllers\API;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\UpdateUserRequest;
use App\Http\Requests\Auth\UserLoginRequest;
use App\Http\Requests\Auth\UserRegisterRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sanctum')->only(['logout', 'update', 'me']);
    }
    
    public function register(UserRegisterRequest $request)
    {
        $user = User::create([
            'name' => $request->name,
            'username' => $request->username,
            'email' => $request->email,
            'phone' => $request->phone,
            'password' => Hash::make($request->password),
        ]);
        
        $tokenResult = $user->createToken('authToken')->plainTextToken;
        
        return ResponseFormatter::success(
            [
                'access_token' => $tokenResult,
                'token_type' => 'Bearer',
                'user' => $user
            ],
            'User berhasil didaftarkan'
        );
    }
    
    public function login(UserLoginRequest $request)
    {
        $credentials = request(['email', 'password']);
        if (!auth()->attempt($credentials)) {
            return ResponseFormatter::error(
                [
                    'message' => 'Unauthorized'
                ],
                'Email atau password salah',
                500
            );
        }
        
        $user = User::where('email', $request->email)->first();
        $tokenResult = $user->createToken('authToken')->plainTextToken;
        
        return ResponseFormatter::success(
            [
                'access_token' => $tokenResult,
                'token_type' => 'Bearer',
                'user' => auth()->user()
            ],
            'User berhasil login'
        );
    }
    
    public function logout(Request $request)
    {
        $token = $request->user()->currentAccessToken()->delete();
        
        return ResponseFormatter::success(
            $token,
            'User berhasil logout',
        );
    }
    
    public function update(UpdateUserRequest $request)
    {
        $user = $request->user();
        
        if ($request->password) {
            $user->update([
                'name' => $request->name ? $request->name : $user->name,
                'username' => $request->username ? $request->username : $user->username,
                'email' => $request->email ? $request->email : $user->email,
                'phone' => $request->phone ? $request->phone : $user->phone,
                'password' => Hash::make($request->password),
            ]);
        } else {
            $user->update($request->validated());
        }
        
        return ResponseFormatter::success(
            $user,
            'Data user berhasil diperbarui'
        );
    }
    
    public function me(Request $request)
    {
        return ResponseFormatter::success(
            $request->user(),
            'Data user berhasil didapatkan'
        );
    }
}
