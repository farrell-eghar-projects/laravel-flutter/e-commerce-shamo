<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory, SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'description',
        'price',
        'tags',
        'category_id',
    ];
    
    public function productGalleries()
    {
        return $this->hasMany(ProductGallery::class);
    }
    
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    
    public function transactionItem()
    {
        return $this->hasOne(TransactionItem::class);
    }
}
