<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use HasFactory, SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'address',
        'shipping_price',
        'total_price',
        'payment',
        'status',
        'user_id',
    ];
    
    public function transactionItems()
    {
        return $this->hasMany(TransactionItem::class);
    }
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
