<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransactionItem extends Model
{
    use HasFactory;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'quantity',
        'user_id',
        'product_id',
        'transaction_id',
    ];
    
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
    
    public function transaction()
    {
        return $this->belongsTo(Transaction::class);
    }
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
