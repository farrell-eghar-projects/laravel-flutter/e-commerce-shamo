<?php

use App\Http\Controllers\API\CategoryController;
use App\Http\Controllers\API\ProductController;
use App\Http\Controllers\API\TransactionController;
use App\Http\Controllers\API\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

route::group(['prefix' => 'auth'], function () {
    Route::post('register', [UserController::class, 'register'])->name('api.auth.register');
    Route::post('login', [UserController::class, 'login'])->name('api.auth.login');
    Route::post('logout', [UserController::class, 'logout'])->name('api.auth.logout');
    Route::post('update', [UserController::class, 'update'])->name('api.auth.update');
    Route::post('me', [UserController::class, 'me'])->name('api.auth.me');
});

Route::apiResource('category', CategoryController::class)->only('index');
Route::apiResource('product', ProductController::class)->only('index');
Route::apiResource('transaction', TransactionController::class)->only(['index', 'store']);